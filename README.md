# cvhash

Use OpenCV to produce a perceptual hash of an image (like pHash)

## Building
### Dependencies
You will need cmake and the development files for OpenCV. The following should be enough for Ubuntu:
```bash
apt-get install build-essential cmake libopencv-dev
```
### Build Instructions
```bash
git clone https://github.com/ls64/cvhash
cd cvhash
mkdir build
cd build
cmake ..
make
```

## Running
```bash
./cvhash your_image.png
```

## Details
This is essentially an OpenCV implementation of http://www.hackerfactor.com/blog/?/archives/432-Looks-Like-It.html

It will produce a 64-bit perceptual hash of an image. This hash can be used to compare the image to others (for applications such as similarity searches).
