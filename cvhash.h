#ifndef CVHASH_H
#define CVHASH_H
#include <string>
#include <iostream>
#include <bitset>
#include <opencv2/opencv.hpp>

// the idea for this class came from http://www.hackerfactor.com/blog/?/archives/432-Looks-Like-It.html
// my implementation uses OpenCV to do most of the work
// it can produce a perceptual hash like pHash (http://www.phash.org/) that can be used to compare images
class cvhash {
	private:
		cv::Mat image; // the original image matrix
		cv::Mat frequencies; // resulting matrix from DCT
		std::bitset<64> qhash;

		// compute the DCT of the loaded image
		void dct() {
			// create a copy of the image
			cv::Mat temporary = image.clone();
			// convert the image to grayscale
			cv::cvtColor(temporary, temporary, CV_BGR2GRAY);
			// resize the matrix to 32x32
			cv::resize(temporary, temporary, cv::Size(32,32));
			// convert the data to a floating point matrix
			cv::Mat fdata(temporary.rows, temporary.cols, CV_32F);
			temporary.convertTo(fdata, CV_32F);
			// perform the DCT
			cv::dct(fdata, frequencies);
		}

		// calculates the 64-bit perceptual hash of the loaded image and stores it in qhash
		std::bitset<64> hash() {
			qhash.reset();
			// cut the frequencies matrix to the first 8x8 pixels
			cv::Mat cut = frequencies(cv::Range(0,8), cv::Range(0,8));

			// calculate the mean value (excluding pixel 0,0)
			float first = cut.at<float>(0,0); // store the first pixel
			cut.at<float>(0,0) = 0; // set the first pixel to zero so it doesn't mess up the average
			float average = mean(cut)[0]; // get the average for the cut matrix
			cut.at<float>(0,0) = first; // restore the first pixel

			assert(cut.rows == cut.cols); // matrix must be square
			int n = cut.rows; // the height of the matrix
			int c = 0; // c is the current bit
			// this is similar to http://stackoverflow.com/a/1779242, but that version starts every slice from the upper-right side
			for(int slice = 0; slice < 2 * n - 1; ++slice) {
				int z = slice < n ? 0 : slice - n + 1;
				for(int j = slice % 2 == 0 ? slice - z : z;
						slice % 2 == 0 ? j >= z : j <= slice - z;
						slice % 2 == 0 ? --j : ++j) {
					if(cut.at<float>(j, slice - j) <= average) {
						qhash.set(c);
					}
					c++;
				}
			}
			return qhash;
		}

	public:
		cvhash() {}

		~cvhash() {}

		// load file and perform DCT
		cvhash(std::string file, bool update = true) {
			load(file);
			if(update) compute();
		}

		// load an existing hash
		cvhash(std::bitset<64> input): qhash(input) {}

		// load an existing OpenCV Mat and perform DCT
		cvhash(cv::Mat input, bool update = true): image(input) {
			if(update) compute();
		}

		// computes the DCT and the hash
		void compute() {
			dct();
			hash();
		}

		// load the image matrix from a file
		bool load(std::string file) {
			image = cv::imread(file, 1);
			if(!image.data)
				return false;
			return true;
		}

		// fast way to get weighted difference between two hashes
		// more significant bits matter more
		// this is a "weighted" difference between the hashes
		// it is calculated by XORing the two bitsets
		std::bitset<64> compare(std::bitset<64> other) const {
			return qhash^other;
		}

		// returns the number of bits that differ
		// according to wikipedia, this is a Hamming distance
		// this is an "unweighted" difference between the hashes
		unsigned hamming(std::bitset<64> other) const {
			unsigned dist = 0;
			for(unsigned c = 0; c < 64; c++) {
				if(!(qhash[c] == other[c])) dist++;
			}
			return dist;
		}

		// converts the 64-bit perceptual hash integer to a string
		std::string hash2string() const {
			std::string str = "";
			for(unsigned c = 0; c < 64; c++) {
				if(qhash[c]) str += "1";
				else str += "0";
			}
			return str;
		}

		// returns an unsigned 64-bit int that contains the bits from
		// the bitset<64>
		uint64_t hash2uint() const {
			return qhash.to_ullong();
		}

		// returns the actual bitset
		std::bitset<64> getset() const {
			return qhash;
		}

		// start an image viewer for the loaded image
		void view_image() const {
			cv::imshow("Image", image);
			cv::waitKey(0);
		}

		// start an image viewer for the DCT frequencies matrix
		// it will be resized to 256x256 for viewing
		void view_frequencies() const {
			// convert the format from floating point to something that can be properly displayed
			cv::Mat converted(frequencies.rows, frequencies.cols, CV_8U);
			frequencies.convertTo(converted, CV_8U);
			// reisze the matrix to 256x256 without using any interpolation
			cv::resize(converted, converted, cv::Size(256,256), 0, 0, cv::INTER_AREA);
			cv::imshow("DCT", converted);
			cv::waitKey(0);
		}

		friend std::ostream& operator<<(std::ostream& os, const cvhash&);
};

// outputs a string of 64 1s or 0s representing the bitset<64>
std::ostream& operator<<(std::ostream& os, const cvhash& cvhash) {
	os << cvhash.hash2string();
	return os;
}
#endif
