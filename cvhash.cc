#include <iostream>
#include "cvhash.h"

int main(int argc, char** argv) {
	if(argc < 2) {
		std::cerr << "Usage: cvhash <image>" << std::endl;
		return 1;
	} else {
		for(int i = 1; i < argc; i++) {
			try {
			cvhash x(argv[i]);
				std::cout << x << "  " << argv[i] << std::endl;
			} catch(...) {
				std::cerr << "ERROR  " << argv[i] << std::endl;
			}
		}
	}
	return 0;
}
